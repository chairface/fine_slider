//
//  FineSlider.h
//  FineSlider
//
//  Created by Charlie Reiman on 5/6/13.
//
// Copyright (c) 2013 Charlie Reiman.
//
// Permission to use, copy, modify, and/or distribute this software for
// any purpose with or without fee is hereby granted, provided that the
// above copyright notice and this permission notice appear in all
// copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND ISC DISCLAIMS ALL WARRANTIES WITH
// REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL ISC BE LIABLE FOR ANY
// SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT

#import <UIKit/UIKit.h>

@interface FineSlider : UISlider

// The yDisplacement is guaranteed to be >= 1. It will never be zero
// and never be negative. You can divide by it freely. Return a positive
// value <= 1.0 for reasonable fine-grained control. 
- (void) setScalingFunction:(float (^)(float yDisplacement))scalingBlock;

@end
