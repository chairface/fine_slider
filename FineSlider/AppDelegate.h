//
//  AppDelegate.h
//  FineSlider
//
//  Created by Charlie Reiman on 5/3/13.
//  Copyright (c) 2013 Charlie Reiman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
