//
//  ViewController.m
//  FineSlider
//
//  Created by Charlie Reiman on 5/3/13.
//  Copyright (c) 2013 Charlie Reiman. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.fineSliderView addTarget:self
                            action:@selector(updateValue:withEvent:)
                  forControlEvents:UIControlEventValueChanged];
}

- (void) updateValue:(FineSlider*)sender withEvent:(UIEvent*)event
{
    self.sliderValue.text = [NSString stringWithFormat:@"%g", sender.value];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
