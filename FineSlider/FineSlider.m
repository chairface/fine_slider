//
//  FineSlider.m
//  FineSlider
//
//  Created by Charlie Reiman on 5/6/13.
//
// Copyright (c) 2013 Charlie Reiman.
//
// Permission to use, copy, modify, and/or distribute this software for
// any purpose with or without fee is hereby granted, provided that the
// above copyright notice and this permission notice appear in all
// copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND ISC DISCLAIMS ALL WARRANTIES WITH
// REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL ISC BE LIABLE FOR ANY
// SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT

// Note: This technique will suppress many of the UIControlEvent messages.
// If there are ones you need besides valueChanged you will probably
// need to manually add them.

#import "FineSlider.h"

@interface FineSlider ()
{
    float _lastX, _relX;
    float (^_scalingBlock)(float yDisplacement);
    UIImage *_normalImage;
}
@end

@implementation FineSlider

- (void) commonInit
{
    // Default scaling function provides reasonable control.
    _scalingBlock = ^(float yDisplacement) {
        float scaleX = 20.0f/(yDisplacement+29.0f);
        return scaleX;
    };
}

- (void) setScalingFunction:(float (^)(float yDisplacement))scalingBlock
{
    _scalingBlock = scalingBlock;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

// Pos is in my local coordinates.
- (void) setValueBasedOnX:(CGFloat)pos
{
    CGRect sliderBounds = self.bounds;
    CGFloat newVal;

    if (pos < CGRectGetMinX(sliderBounds)) {
        newVal = self.minimumValue;
    }
    else if (pos > CGRectGetMaxX(sliderBounds)) {
        newVal = self.maximumValue;
    }
    else {
        CGFloat spread = self.maximumValue - self.minimumValue;
        CGFloat fract = pos - CGRectGetMinX(sliderBounds);
        fract /= CGRectGetWidth(sliderBounds);
        newVal = self.minimumValue + (fract * spread);
    }
    if (self.value != newVal) {
        self.value = newVal;
        // valueChanged does not get sent automatically except when you
        // release your finger.
        [self sendActionsForControlEvents:UIControlEventValueChanged];
    }
}

// On first event, we must set the relative and last values
// so tracking works.
- (BOOL) beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
    CGPoint touchPoint = [touch locationInView:self];

    _relX = _lastX = touchPoint.x;
    [self setValueBasedOnX:touchPoint.x];

    // Fix thumb highlighting
    _normalImage = [self thumbImageForState:UIControlStateNormal];
    UIImage *highlightThumb = [self thumbImageForState:UIControlStateHighlighted];
    [self setThumbImage:highlightThumb forState:UIControlStateNormal];
    return YES;
}

// Here's where all the interesting code lives. We need to compute a vertical
// displacement which we can use to create a horizonal scaling. This
// scaling is applied to any relative horizontal movement, which is
// then added to an invisible relative horizontal cursor. This
// relative cursor is what we use to set the actual slider value
// via setValueBasedOnX.
- (BOOL) continueTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
    CGPoint touchPoint = [touch locationInView:self];
    CGFloat vertDisplacement=0.0;

    // vertDisplacement is the veritcal distance of the touch from
    // our bounding box. It is always positive or zero at this point.
    if (touchPoint.y < CGRectGetMinY(self.bounds)) {
        vertDisplacement = CGRectGetMinY(self.bounds) - touchPoint.y;
    }
    else if (touchPoint.y > CGRectGetMaxY(self.bounds)) {
        vertDisplacement = touchPoint.y - CGRectGetMaxY(self.bounds);
    }

    // If vertD is less than 1, we assume it's in our bounds. So just
    // use a normal, absolute positioned, set. This also need to
    // reset the lastX and relative cursor so we can still track when
    // we possible escape the bounds on future movements.
    if (vertDisplacement < 1.0) {
        _relX = _lastX = touchPoint.x;
        [self setValueBasedOnX:touchPoint.x];
    }
    else {
        // We had movement outside our bounds.
        // All this math is experimentally tuned. Feel free to experiment by
        // using your own scaling function.
        CGFloat scaleX = _scalingBlock(vertDisplacement);
        // Compute our delta X
        CGFloat deltaX = touchPoint.x - _lastX;
        deltaX *= scaleX;
        // And update our relative cursor
        _relX += deltaX;
        // Clip things so they're in our horizontal bounds
        _relX = fminf(_relX, CGRectGetMaxX(self.bounds));
        _relX = fmaxf(_relX, CGRectGetMinX(self.bounds));
        // Finally set the value and prepare to track the next relative movment.
        [self setValueBasedOnX:_relX];
        _lastX = touchPoint.x;
    }
    return YES;
}

// This needs to be swallowed or the thumb will jump when you release
// your finger. 
- (void) endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
    // Restore thumb to normal
    [self setThumbImage:_normalImage forState:UIControlStateNormal];
    _normalImage = nil;
}

@end
